**Navbanner Options Description:**
    Allows administrators to set alternative options within the site banner

**Usage:** ADMINS ONLY

Add one or more of the following to the Banner/Logo box in the Site (admin) page.

Navigation: left dropdown (Profile) menu > Admin > Site > Banner/Logo

    eg. full_name
    eg. channel_name full_name
    eg. This channel belongs to channel_name.. their email is account_email


| Option | Description |
| ------ | ------ |
| channel_address | Shows the channel email address eg. channelname@domain |
| channel_name | Shows the channel name |
| full_name | Shows the full name of the user |
| account_email | Shows the full email address of the user |
| host_name | The domain of the server eg. start.hubzilla.org |
| site_name | The name of the Hub.. see the 'site name' field, within the Site (admin) page |
| service_class | The service class of the user, useful for extended entries/ newly registered user |
| account_level | The account level of the user, again, useful for extended entries/ newly registered users |